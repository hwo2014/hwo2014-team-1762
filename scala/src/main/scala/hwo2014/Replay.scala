package hwo2014

import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

/** Replay race from log file. */
object Replay extends App {

  val path = new File("racedata")

  val latestFile = path.listFiles().toList.sorted.last
  val reader = new BufferedReader(new InputStreamReader(new FileInputStream(latestFile), "UTF8"))
  val protocol = new Protocol(reader, None, None)

  val data = {
    var raceData: RaceData = null
    var running = true
    var throttle = 0.0
    while (running) protocol.processInput {
      case MsgGameInit(data) => raceData = new RaceData(data)
      case MsgCarPositions(pos,tick) =>
        raceData.collect(throttle, pos, tick)
        // only beginning of track
        if (pos(raceData.myColor).pieceIndex == 39) running = false
      case MsgEOF() => running = false //assert(false)
      case MsgThrottle(data) =>
        throttle = data
      //        println("Throttle: " + throttle)

      case _ =>
    }
    raceData
  }
  val vis = new Visualizer(data)
  vis.visualize()

  // test estimator
  val estimator = new SimpleEstimator
  estimator.init(data)
  for (tick <- 1 to data.getTick) {
    estimator.dataUpdated(tick - 1)
    val lastPos = data.snapshots(tick - 1).positions(data.myColor)
    val actualPos = data.snapshots(tick).positions(data.myColor)
    val throttle = data.snapshots(tick).lastThrottle
    val estimatedPos = estimator.estimate(throttle)
    val estimatedVel = data.distance(lastPos, estimatedPos)
    val actualVel = data.distance(lastPos, actualPos)
    val piece = data.track.pieces(actualPos.pieceIndex)

    vis.drawDotHalf(vis.colorOct(0xf88), tick, -(estimatedPos.angle - lastPos.angle) / 45 * 10)

    import Util.format
    def posInfo(pos: Position) = "[" + pos.pieceIndex + " " + data.track.pieces(pos.pieceIndex) + "]"
    def info(estimated: Double, actual: Double) =
      "[est: " + format(estimated, 4, 8) +
        ", act: " + format(actual, 4, 8) +
        ", dif: " + format(estimated - actual, 4, 8) + "]"
    println("T+" + tick +
      " Throttle " + throttle +
      ", Vel " + info(estimatedVel, actualVel) +
      ", Angle diff " + info(estimatedPos.angle - lastPos.angle, actualPos.angle - lastPos.angle) +
      ", curv " + format(piece.curvature, 4) +
      " in " + posInfo(actualPos))
  }

  val view = new View("Replay", vis.image, 1)

}
