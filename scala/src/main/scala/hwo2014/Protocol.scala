package hwo2014

import java.io.BufferedReader
import java.io.File
import java.io.PrintWriter

import org.json4s.DefaultFormats
import org.json4s.Extraction
import org.json4s.JArray
import org.json4s.JField
import org.json4s.JInt
import org.json4s.JObject
import org.json4s.JString
import org.json4s.JValue
import org.json4s.jvalue2extractable
import org.json4s.jvalue2monadic
import org.json4s.native.Serialization
import scala.collection.mutable

abstract class Message

/** End of stream / file. */
case class MsgEOF() extends Message
abstract class ServerMessage extends Message
abstract class ClientMessage(val msgType: String) extends Message

// ---- server messages
case class MsgGameInit(trackData: TrackData) extends ServerMessage
case class MsgCarPositions(positions: Map[String, Position],gameTick: Int) extends ServerMessage
//case class MsgYourCar(color: String) extends ServerMessage
case class MsgGameStart() extends ServerMessage
case class MsgGameEnd(data: JValue) extends ServerMessage // TODO: parse value
case class MsgTournamentEnd() extends ServerMessage
case class MsgTurboAvailable(turboDurationMilliseconds: Double, turboDurationTicks: Int, turboFactor: Double) extends ServerMessage
case class MsgTurboStart(name: String,color: String,gameTick: Int) extends ServerMessage 
case class MsgTurboEnd(name: String,color: String,gameTick: Int) extends ServerMessage
case class MsgLapFinished(data: JValue) extends ServerMessage // TODO: parse value
case class MsgDNF(carColor: String, reason: String) extends ServerMessage
case class MsgFinish(carColor: String) extends ServerMessage
case class MsgCrash(carColor: String) extends ServerMessage
case class MsgSpawn(carColor: String) extends ServerMessage
case class MsgUndefined(msgType: String, value: JValue) extends ServerMessage

// ---- client messages
case class MsgTurbo(data: String) extends ClientMessage("turbo")
case class MsgJoin(name: String, key: String) extends ClientMessage("join")
case class MsgJoinRace(botId: Map[String, String], trackName: String,carCount: Int,password: String) extends ClientMessage("joinRace")
case class MsgThrottle(data: Double) extends ClientMessage("throttle") {
  override val msgType = "throttle"
}
case class MsgSwitch(data: String) extends ClientMessage("switchLane")

class Protocol(reader: BufferedReader, writer: Option[PrintWriter], logger: Option[DataLogger]) {
  implicit val formats = new DefaultFormats {}
  var carColor: String = null
  val crashedCars = mutable.Set[String]()

  def send(msgType: String, data: JValue) = sendInternal(new MsgWrapper(msgType, data))
  def send(msg: ClientMessage) = sendInternal(msg)
  def send(msg: MsgWrapper) = sendInternal(msg)
  def send(msg: Map[String,Any]) = sendInternal(msg)

  def sendInternal(msg: AnyRef) {
    assert(writer.isDefined, "Protocol has no defined output")
    val line = Serialization.write(msg)
    logger.map(_.write(line))
    writer.get.println(line)
    writer.get.flush
  }

  /**
   * Parse and relay single server message.
   */
  def processInput(listener: Message => Unit) = {
    val line = reader.readLine()
    // last applied throttle
    if (line == null) {
      listener(new MsgEOF())
    } else {
      logger.map(_.write(line))
      parseMessage(line).map(listener)
    }
  }

  def parseMessage(line: String): Option[Message] = {
    Serialization.read[MsgWrapper](line) match {
      case MsgWrapper("yourCar", data,_) =>
        carColor = (for {
          JObject(field) <- data
          JField("color", JString(color)) <- field
        } yield color).head
        // yourCar message is included in TrackData object
        //listener(new MsgYourCar(carColor))
        None
      case MsgWrapper("gameInit", JObject(data),_) =>
        val lanes = (for {
          JField("race", JObject(race)) <- data
          JField("track", JObject(track)) <- race
          JField("lanes", JArray(lanes)) <- track
          JObject(lane) <- lanes
          JField("index", JInt(index)) <- lane
          JField("distanceFromCenter", JInt(distance)) <- lane
        } yield (index, distance.toDouble)).sortWith(_._1 < _._1).map(_._2).toIndexedSeq
        val pieces = for {
          JField("race", JObject(race)) <- data
          JField("track", JObject(track)) <- race
          JField("pieces", JArray(pieces)) <- track
          piece <- pieces
        } yield {
          val length = (piece \ "length").extractOrElse[Double](0)
          val switch = (piece \ "switch").extractOrElse[Boolean](false)
          val radius = (piece \ "radius").extractOrElse[Double](0)
          val angle = (piece \ "angle").extractOrElse[Double](0)
          if (length > 0) new StraightPiece(switch, length)
          else new BendPiece(switch, radius, angle)
        }
        val cars = for {
          JField("race", JObject(race)) <- data
          JField("cars", JArray(cars)) <- race
          car <- cars
        } yield {
          val id = car \ "id"
          val name = (id \ "name").extract[String]
          val color = (id \ "color").extract[String]
          val dimensions = car \ "dimensions"
          val length = (dimensions \ "length").extract[Double]
          val width = (dimensions \ "width").extract[Double]
          val guidePos = (dimensions \ "guideFlagPosition").extract[Double]
          new Car(name, color, length, width, guidePos)
        }
        val trackPieces = pieces.toIndexedSeq
        trackPieces.indices.foreach { i =>
          trackPieces(i).prev = trackPieces((i + pieces.length - 1) % trackPieces.length)
          trackPieces(i).next = trackPieces((i + 1) % trackPieces.length)
        }
        val carMap = cars.map(c => (c.color, c)).toMap
        assert(carColor != null, "carColor message not processed")
        
        val indices = trackPieces.indices.filter(trackPieces(_).hasSwitch).distinct
        val sections : IndexedSeq[TrackSection] = (indices.last +: indices, indices).zipped.map { (a, b) =>
          val range = if (a <= b) { a to b } else { (a to trackPieces.indices.last) ++ (0 to b) }
	  val pieces = range.map(trackPieces(_))
          new TrackSection(range,lanes.indices.map { laneIndex =>
     	    new SectionLane(pieces.map(_.length(lanes,laneIndex)).sum,pieces.map(_.curvature).sum/pieces.length)
          })
	}
	for (sectionIndex <- sections.indices) {
          val section = sections(sectionIndex)
          section.prev = sections((sectionIndex + sections.length -1) % sections.length)
          section.next = sections((sectionIndex + 1) % sections.length)
	  for(i <- section.range) pieces(i).section = section
	  println("Section: " + section.range.head + " to " + section.range.tail)
	  for (option <- section.lanes) {
	    println("  " + option.length)
	  }
	}
        val trackData = new TrackData(trackPieces, lanes, carMap, carMap(carColor), sections)
        Some(new MsgGameInit(trackData))
      case MsgWrapper("carPositions", JArray(list), gameTick) =>
        val positions = for (car <- list) yield {
          val color = (car \ "id" \ "color").extract[String]
          val angle = (car \ "angle").extract[Double]
          val pieceIndex = (car \ "piecePosition" \ "pieceIndex").extract[Integer]
          val inPieceDistance = (car \ "piecePosition" \ "inPieceDistance").extract[Double]
          val startLane = (car \ "piecePosition" \ "lane" \ "startLaneIndex").extract[Int]
          val endLane = (car \ "piecePosition" \ "lane" \ "endLaneIndex").extract[Int]
          val state = if (crashedCars(color)) CarState.Crashed else CarState.Active
          (color, Position(angle, pieceIndex, inPieceDistance, startLane, endLane, state))
        }
        Some(new MsgCarPositions(positions.toMap,gameTick.getOrElse(0)))
      case MsgWrapper("gameStart", _,_) =>
        Some(new MsgGameStart())
      case MsgWrapper("gameEnd", data,_) =>
        Some(new MsgGameEnd(data))
      case MsgWrapper("tournamentEnd", _,_) =>
        Some(new MsgTournamentEnd())
      case MsgWrapper("turboAvailable", data,_) =>
        Some(new MsgTurboAvailable((data \ "turboDurationMilliseconds").extract[Double], (data \ "turboDurationTicks").extract[Int], (data \ "turboFactor").extract[Double]))
      case MsgWrapper("turboStart", data, gameTick) =>
        Some(new MsgTurboStart((data \ "name").extract[String],(data \ "color").extract[String],gameTick.getOrElse(0)));
      case MsgWrapper("turboEnd", data, gameTick) =>
        Some(new MsgTurboEnd((data \ "name").extract[String],(data \ "color").extract[String],gameTick.getOrElse(0)));
      case MsgWrapper("lapFinished", data,_) =>
        Some(new MsgLapFinished(data))
      case MsgWrapper("dnf", data,_) =>
        Some(new MsgDNF((data \ "car" \ "color").extract[String], null))
      case MsgWrapper("finish", data,_) =>
        Some(new MsgFinish((data \ "color").extract[String]))
      case MsgWrapper("crash", data,_) =>
        val color = (data \ "color").extract[String]
        crashedCars.add(color)
        Some(new MsgCrash(color))
      case MsgWrapper("spawn", data,_) =>
        val color = (data \ "color").extract[String]
        crashedCars.remove(color)
        Some(new MsgSpawn(color))
      // -- client messages
      case MsgWrapper("throttle", data,_) =>
        Some(new MsgThrottle(data.extract[Double]))
      // -- unknown
      case MsgWrapper(msgType, value,_) =>
        Some(new MsgUndefined(msgType, value))
    }
  }
}

case class MsgWrapper(msgType: String, data: JValue, gameTick: Option[Int] = None)

object MsgWrapper {
  implicit val formats = new DefaultFormats {}

  def apply(msgType: String, data: Any, gameTick: Int): MsgWrapper = {
    new MsgWrapper(msgType, Extraction.decompose(data), Some(gameTick))
  }

  def apply(msgType: String, data: Any): MsgWrapper = {
    new MsgWrapper(msgType, Extraction.decompose(data), None)
  }

}
