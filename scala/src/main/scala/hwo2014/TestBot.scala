package hwo2014

object TestBot extends App {
  //  args.toList match {
  //    case hostName :: port :: botName :: botKey :: _ =>
  //      new BotMain(hostName, Integer.parseInt(port), botName, botKey, new DefaultAI())
  //    case _ => println("args missing")
  //  }

  val hostName = "testserver.helloworldopen.com"
  val port = 8091
  val botName = "Moon Rover"
  val botKey = "Mn/5QsMnlXyjuw"
  val protocol = BotMain.join(hostName, port, botName, botKey)
  // val protocol = BotMain.create(hostName, port, botName, botKey, "usa")
  new BotMain(protocol, new TestAI(true))
}

class TestAI(verbose: Boolean) extends AI {
  var data: RaceData = null
  val estimator = new SimpleEstimator

  var vis: Visualizer = null
  var view: View = null

  def gameInit(data: RaceData) {
    this.data = data
    estimator.init(data)
    if (verbose) {
      vis = new Visualizer(data)
      view = new View("Race", vis.image, 1)
    }
  }

  def log(message: String) = if (verbose) println(message)

  /** Estimated max velocity for a track piece. */
  def maxVel(piece: TrackPiece) = piece match {
    case StraightPiece(_, _)     => 6 //Double.PositiveInfinity
    case BendPiece(_, radius, _) => estimator.slipVelocity(radius)
  }

  def action(): Action = {
    estimator.dataUpdated(data.getTick)
    if (verbose) {
      vis.visualize()
      view.repaint()
    }
    if (!estimator.validEstimates) {
      ActionThrottle(1)
    } else if (data.lastPos.state == CarState.Crashed) {
      ActionThrottle(1)
    } else {
      val pieces = data.nextPieceIndexes
      val currentPiece = data.track.pieces(pieces.head)
      var breakDistance = Double.PositiveInfinity
      var processedMaxVel = Double.PositiveInfinity
      var shouldBrake = false
      for (i <- pieces.tail) {
        val piece = data.track.pieces(i)
        val maxPieceVel = maxVel(piece)
        if (maxPieceVel < processedMaxVel) {
          processedMaxVel = maxPieceVel
          val (limitDist, ticks) = estimator.downUntilVelocity(estimator.velTan, maxPieceVel)
          val lane = data.lastPos.beginLane
          val dist = data.distance(data.lastPos, new Position(0, i, 0, lane, lane, CarState.Active))
          if (limitDist >= dist) {
            shouldBrake = true
            log("[" + pieces.head + "] Should brake for " + i + ", " + piece)
          }
        }
      }
      if (shouldBrake) {
        ActionThrottle(0)
      } else {
        val throttle = estimator.throttleForVelocityClamp(estimator.velTan, maxVel(currentPiece))
        ActionThrottle(throttle)
      }
    }
  }

}
