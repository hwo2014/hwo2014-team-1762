package hwo2014

/** Test exact formulas. */
object SimTest extends App {

  val est = new SimpleEstimator
  est.maxVel = 10
  est.slack = 0.02

  var lastPos = 0.0
  var pos = 0.0

  def step(throttle: Double) {
    val vel = pos - lastPos
    val newVel = vel + (throttle * est.maxVel - vel) * est.slack

    // update state
    lastPos = pos
    pos = pos + newVel
  }

  val targetPos = 300
  val targetVel = 5

  step(1)
  var brake = false
  for (i <- 0 until 100) {
    val vel = pos - lastPos

    val (dist, ticks) = est.downUntilVelocity(vel, targetVel)
    if (!brake && dist > targetPos - pos) brake = true

    val throttle =
      if (brake) 0
      else 1
    println("Pos: " + Util.format(pos, 2, 6) + ", vel: " + Util.format(vel, 2, 5) + ", throttle: " + throttle + ", downUntil: " + dist + ", " + ticks)

    step(throttle)

  }

}
