package hwo2014

import scala.collection.mutable.ArrayBuffer

/**
 * Given current track, car position history and throttle, estimates car position after next tick.
 */
abstract class Estimator() {
  var data: RaceData = null

  /** Initialize estimator for new track configuration. */
  def init(data: RaceData) {
    this.data = data
  }

  def dataUpdated(tick: Int)

  /** Estimate next position with given throttle. */
  def estimate(throttle: Double): Position

}

class SimpleEstimator extends Estimator {

  /** Current car position data. */
  var position: Position = null
  /** Current car tangential angle in radians. */
  var angle = 0.0
  /** Current track piece. */
  var piece: TrackPiece = null

  /** Current tangential velocity. */
  var velTan = 0.0
  /** Current radial velocity. */
  var velRad = 0.0
  /** Estimated car mass. */
  //  var mass = 1.0
  /** Estimated maximum velocity. */
  var maxVel = 0.0
  /** Estimated throttle slack. */
  var slack = 0.0
  /** Limit factor when car angle starts to increase (limit=vel^2/radius).*/
  var slipLimit = 0.32
  /** Estimated angle where car still won't crash. */
  var stableAngle = 0.0

  override def dataUpdated(tick: Int) = {
/*    if (data.turboFactor != 1)
      data.turboDuration--
    if (data.turboDuration <= 0)
      data.turboFactor = 1*/
    val snap0 = data.snapshots(tick)
    val pos0 = snap0.positions(data.myColor)
    position = pos0
    angle = Util.toRadians(pos0.angle)
    piece = data.track.pieces(pos0.pieceIndex)
    if (tick > 0) {
      val snap1 = data.snapshots(tick - 1)
      val pos1 = snap1.positions(data.myColor)
      velTan = data.distance(pos1, pos0)
      //      velRad = data.angleDiff(pos1, pos0)
      velRad = pos0.angle - pos1.angle

      if (!validEstimates) estimateLatents(tick)

      //      val oldAngle = stableAngle
      stableAngle = (stableAngle :: snap0.positions.map(_._2).filter(_.state == CarState.Active).map(_.angle).toList).max
      //      if (oldAngle != stableAngle) {
      //        println("Found new stable angle: " + stableAngle)
      //      }

    }
  }

  /** Estimate maxVel and slack */
  def estimateLatents(tick: Int) = {
    if (tick > 2) {
      val snap0 = data.snapshots(tick - 0)
      val snap1 = data.snapshots(tick - 1)
      val snap2 = data.snapshots(tick - 2)
      val snap3 = data.snapshots(tick - 3)
      val t0 = snap1.lastThrottle
      val t1 = snap0.lastThrottle

      val p00 = snap3.positions(data.myColor).inPieceDistance
      val p0 = snap2.positions(data.myColor).inPieceDistance
      val p1 = snap1.positions(data.myColor).inPieceDistance
      val p2 = snap0.positions(data.myColor).inPieceDistance
      val v0 = p0 - p00
      val v1 = p1 - p0
      val v2 = p2 - p1
      maxVel = (v1 * v1 - v0 * v2) / (t1 * (v1 - v0) - t0 * (v2 - v1))
      slack = (v2 - v1) / (t1 * maxVel - v1)
      if (t0==0 || t1==0 || maxVel.isInfinite() || maxVel == 0 || slack.isInfinite() || slack == 0) {
        println("Failed to estimate params, maxVel " + maxVel + ", slack " + slack + ", throttles " + (t0, t1) + ", velocities " + (v0, v1, v2))
        maxVel = 0
        slack = 0
      } else {
        println("MaxVel: " + maxVel + ", slack: " + slack)
      }
    }
  }

  /** Are default track parameter estimates valid. */
  def validEstimates = maxVel > 0

  /** Calculate limiting velocity where car angle starts to increase. */
  def slipVelocity(radius: Double): Double = Math.sqrt(slipLimit * radius)

  /** Calculate throttle for reaching exact target velocity. */
  def throttleForVelocity(currentVel: Double, targetVel: Double): Double = Util.clamp((targetVel + (slack - 1) * currentVel) / (slack * maxVel))

  /** Calculate throttle for reaching exact target velocity, clamped to [0,1]. */
  def throttleForVelocityClamp(currentVel: Double, targetVel: Double): Double = Util.clamp(throttleForVelocity(currentVel, targetVel))

  /** Calculate how long distance / how many ticks for braking to target velocity. */
  def downUntilVelocity(currentVel: Double, targetVel: Double): (Double, Int) = {
    assert(validEstimates, "Track parameters have not been estimated")
    assert(targetVel > 0, "non-positive velocity cannot be reached")
    val throttle = 0
    var vel = currentVel
    var pos = 0.0
    var ticks = 0
    while (vel > targetVel) {
      vel = vel + (throttle * maxVel - vel) * slack
      pos += vel
      ticks += 1
    }
    (pos, ticks)
  }

  override def estimate(throttle: Double): Position = {
    //    val force = throttle * power
    //    val forceTan = Math.cos(angle)
    //    val forceNormal = Math.sin(angle)
    //    val targetVel = throttle * power

    val targetVel = throttle * maxVel
    val posNew = velTan + (targetVel - velTan) * slack

    val pos = data.newPosition(position, posNew, 0)
    // angle difference between current and estimated position
    //    val newAngle = (position.angle + velRad ) *0.3
    // update position with new estimated angle
    //    val newAngle = position.angle - velRad + piece.curvature * 66 * throttle

    val c = velTan - Math.sqrt(0.32 / piece.curvature)
    val targetAngle = c
    val newAngle = position.angle + c
    Position(newAngle, pos.pieceIndex, pos.inPieceDistance, pos.beginLane, pos.endLane, CarState.Active)
  }

}
