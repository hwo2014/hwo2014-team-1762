package hwo2014

import java.util.Locale

object Util {

  def toRadians(degrees: Double) = degrees / 180 * Math.PI
  def toDegrees(radians: Double) = radians * 180 / Math.PI

  def clamp(value: Double): Double = if (value < 0) 0 else if (value > 1) 1 else value

  def format(value: Double, decimals: Int = 2, padding: Int = 0) = {
    //    	  val formatter = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US))
    //        formatter.format(value)
    ("%." + decimals + "f").formatLocal(Locale.ENGLISH, value).reverse.padTo(padding, ' ').reverse
    
  }

}
