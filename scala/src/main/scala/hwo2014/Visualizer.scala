package hwo2014

import java.awt.Graphics2D
import java.awt.image.BufferedImage
import scala.collection.mutable.ListBuffer
import java.awt.Color
import javax.swing.JComponent
import javax.swing.JFrame
import java.awt.Graphics
import java.awt.Dimension
import java.awt.BorderLayout
import scala.util.Random

object Visualizer extends App {
  //	val visualizer = new Visualizer()
  //	new View("Race data", visualizer.image, 1)
  //	val random = new Random
  //	for (i <- 0 until 1024) {
  //	  visualizer.collect(random.nextDouble*0.1+Math.sin(i*0.01))
  //	}
}

class Visualizer(data: RaceData) {
  val colors = List(new Color(128, 128, 128), new Color(255, 0, 0), new Color(0, 255, 0), new Color(128, 128, 255))
  def colorOct(colorOctal: Int) = new Color(((colorOctal >> 8) & 0xf) * 255 / 16, ((colorOctal >> 4) & 0xf) * 255 / 16, (colorOctal & 0xf) * 255 / 16)

  val sizeX = 1024
  val sizeY = 512
  val halfY = sizeY / 2

  val image = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_RGB)
  val gfx = image.getGraphics().asInstanceOf[Graphics2D]

  var lastTick = -1
  var lastPieceIndex = -1

  def visualize() {
    for (x <- lastTick + 1 to data.getTick)
      visualize(x)
  }

  def visualize(tick: Int) {
    lastTick = tick
    val snapshot = data.snapshots(tick)
    val x = tick
    val position = snapshot.positions(data.myColor)
    val pieceIndex = position.pieceIndex
    val piece = data.track.pieces(pieceIndex)
    val green = if (piece.hasSwitch) 128 else 64
    val color = piece match {
      case bend: BendPiece         => new Color(Math.min(255, (10000 / bend.radius) + 50).toInt, green, 0)
      case straight: StraightPiece => new Color(64, green, 128)
    }
    if (lastPieceIndex != pieceIndex) {
      lastPieceIndex = pieceIndex
      drawSeparator(colorOct(0x888), x)
    } else {
      drawSeparator(color, x)
    }

    val velTan = data.velocity(tick)

    drawDotHalf(colorOct(0x888), x, 0)
    drawDot(colorOct(0x8f8), x, snapshot.lastThrottle)
    drawDot(colorOct(0x080), x, velTan * 0.1)
    if (piece.curvature > 0) {
      val limit = Math.sqrt(0.32 / piece.curvature)
      drawDot(colorOct(0x08f), x, limit * 0.1)
    }
    drawDot(colorOct(0x80f), x, velTan * velTan / piece.radius)
    
    drawDotHalf(colorOct(0xf00), x, position.angle / 90)
    //    drawDotHalf(colorOct(0x88f), x, piece.curvature*10)
    drawDotHalf(colorOct(0xff0), x, data.angleDiff(tick) / 45 * 10)

  }

  def drawDotHalf(color: Color, x: Double, y: Double) {
    gfx.setColor(color)
    gfx.fillRect(x.toInt, halfY - (y * halfY).toInt, 1, 1)
  }

  def drawDot(color: Color, x: Double, y: Double) {
    gfx.setColor(color)
    gfx.fillRect(x.toInt, sizeY - (y * sizeY).toInt, 1, 1)
  }

  def drawSeparator(color: Color, x: Double) {
    gfx.setColor(color)
    gfx.fillRect(x.toInt, 0, 1, sizeY)
  }
}

//class RaceView(data: RaceData) {
//  data.track.pieces.zipWithIndex.foreach { (piece: TrackPiece, i: Int) =>
//    data.getPiecePos(i)
//  }
//}

object View {
  def apply(title: String, sizeX: Int, sizeY: Int) = {
    val image = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_RGB)
    new View(title, image, 1)
  }
}

class View(title: String, val image: BufferedImage, scale: Double) extends JComponent {

  val sizeX = (image.getWidth() * scale).intValue()
  val sizeY = (image.getHeight() * scale).intValue()

  setPreferredSize(new Dimension(sizeX, sizeY))

  val frame = new JFrame(title);
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  frame.getContentPane().add(this, BorderLayout.CENTER)
  frame.pack()
  frame.setVisible(true)

  override def paintComponent(g: Graphics) {
    g.drawImage(image, 0, 0, sizeX, sizeY, null)
  }
}