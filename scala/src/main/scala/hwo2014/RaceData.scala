package hwo2014

import scala.collection.mutable.ArrayBuffer
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Locale

class Turbo(val factor: Double,val durationInTicks: Int)

/** For collecting single race data. */
class RaceData(val track: TrackData) {

  /** All collected tick state snapshots. */
  val snapshots = new ArrayBuffer[Snapshot]()
  val ticksPerSecond = 60
  var availableTurbo: Turbo = null
  var activeTurbo: Turbo = null
  var lastTick = 0
  /** Collect next tick snapshot. */
  def collect(throttle: Double, positions: Map[String, Position], gameTick: Int) {
    // TODO: Add turbo to snapshot if needed
    val snapshot = new Snapshot(throttle, positions)
    snapshots += snapshot
    lastTick = gameTick
    // report status
    val logTick = false
    if (logTick && snapshots.length > 1) {
      import Util.format
      val prev = snapshots(snapshots.length - 2).positions(myColor)
      val now = snapshots(snapshots.length - 1).positions(myColor)
      val velocity = distance(prev, now)
      def info(pos: Position) = {
        val piece = track.pieces(pos.pieceIndex)
        "(" + pos.pieceIndex + ", " + format(pos.inPieceDistance) + ", " + format(length(piece, pos.beginLane, pos.endLane)) + " (" + format(length(piece)) + "), " + pos.beginLane + "->" + pos.endLane + "), " + pos.angle
      }
      println("tick " + getTick + ": " + info(now) + ", " + format(velocity))
    }
  }
  def addTurbo(factor: Double,durationTicks: Int) {
    availableTurbo = new Turbo(factor,durationTicks);
  }
  def beginTurbo(color: String) { 
    if (color != myColor) return
    activeTurbo = availableTurbo;
    availableTurbo = null
  }
  def endTurbo(color: String) { 
    if (color != myColor) return
    activeTurbo = null
  }
  def turboAvailable(): Boolean = availableTurbo != null
  def turboActive(): Boolean = activeTurbo != null
  def turboFactor(): Double = if (activeTurbo != null) activeTurbo.factor else 1.0

  def laneRadius(piece: TrackPiece, lane: Int) = piece match {
    case StraightPiece(_, length)    => Double.PositiveInfinity
    case BendPiece(_, radius, angle) => radius - Math.signum(angle) * track.lanes(lane)
  }

  /** Center line piece length */
  def length(piece: TrackPiece) = piece match {
    case StraightPiece(_, length)    => length
    case BendPiece(_, radius, angle) => radius * Math.abs(angle) / 180 * Math.PI
  }
  /** lane length in piece */
  def length(piece: TrackPiece, beginLane: Int, endLane: Int): Double = piece match {
    case StraightPiece(_, length) =>
      def len(x: Double, y: Double) = Math.sqrt(x * x + y * y)
      // TODO: switch piece length is incorrect (= switch lane is not straight)
      if (beginLane == endLane) length else len(length, track.lanes(beginLane) - track.lanes(endLane))
    case BendPiece(_, radius, angle) =>
      if (beginLane == endLane) (radius - Math.signum(angle) * track.lanes(beginLane)) * Math.abs(angle) / 180 * Math.PI
      //TODO: switch lane approximated with simple average
      else (length(piece, beginLane, beginLane) + length(piece, endLane, endLane)) * 0.5
  }
  /** Lane angle in piece, in degrees, relative to piece entry point. */
  def pieceAngle(piece: TrackPiece, inPieceDistance: Double, beginLane: Int, endLane: Int): Double = piece match {
    case StraightPiece(_, length) => 0
    case BendPiece(_, radius, angle) =>
      if (beginLane == endLane) Util.toDegrees(inPieceDistance / (radius - Math.signum(angle) * track.lanes(beginLane)))
      //TODO: switch lane approximated with simple average
      else (pieceAngle(piece, inPieceDistance, beginLane, beginLane) +
        pieceAngle(piece, inPieceDistance, endLane, endLane)) * 0.5
  }
  /** Lane angle in piece, in degrees, relative to piece entry point. */
  def pieceAngle(pos: Position): Double = pieceAngle(track.pieces(pos.pieceIndex), pos.inPieceDistance, pos.beginLane, pos.endLane)

  /** Number of track pieces. */
  val numPieces = track.pieces.length
  /** Cumulative track piece lengths (center line). */
  val cumulativeLengths = track.pieces.scanLeft(0.0)(_ + length(_))
  /** track center length */
  val totalTrackLength = cumulativeLengths(numPieces)

  //  def position(pieceIndex: Int, piecePos: Double, lane: Double): Double = cumulativeLengths(pieceIndex) + piecePos
  //  def position(pos: Position): Double = position(pos.pieceIndex, pos.inPieceDistance, pos.lane)

  /** Actual distance between two car positions. Must by in same or adjacent track pieces. */
  def distance(pos1: Position, pos2: Position): Double = {
    if (pos1.pieceIndex == pos2.pieceIndex) {
      pos2.inPieceDistance - pos1.inPieceDistance
    } else {
      assert((pos1.pieceIndex + 1) % numPieces == pos2.pieceIndex, "Not consecutive track pieces: " + pos1.pieceIndex + ", " + pos2.pieceIndex)
      pos2.inPieceDistance + length(track.pieces(pos1.pieceIndex), pos1.beginLane, pos1.endLane) - pos1.inPieceDistance
    }
  }
  
  /** Actual lane angle difference between two car positions. Must by in same or adjacent track pieces. */
  def angleDiff(pos1: Position, pos2: Position): Double = {
    if (pos1.pieceIndex == pos2.pieceIndex) {
      pieceAngle(pos2) - pieceAngle(pos1)
    } else {
      assert((pos1.pieceIndex + 1) % numPieces == pos2.pieceIndex, "Not consecutive track pieces: " + pos1.pieceIndex + ", " + pos2.pieceIndex)
      pieceAngle(pos2) - pieceAngle(pos1) //+ pos1.angle
    }
  }

  /** Piece positions as position vector and angle pairs. */
  val piecePositions = {
    track.pieces.scanLeft(V2d(0, 0), 0) { (last, piece) =>
      piece match {
        case StraightPiece(_, length)    => last._1 + V2d(length, 0).rot(Util.toRadians(last._2))
        case BendPiece(_, radius, angle) =>
      }
      (V2d(0, 0), 0)
    }
  }

  val myColor = track.myCar.color
  def lastSnapshot = snapshots(snapshots.length - 1)
  /** Last position of my car. */
  def lastPos = lastSnapshot.positions(myColor)

  def nextPieceIndexes(startIndex: Int): IndexedSeq[Int] = (0 until numPieces).map(i => (startIndex + i) % numPieces)

  def nextPieceIndexes(): IndexedSeq[Int] = nextPieceIndexes(lastPos.pieceIndex)

  /** Get all track pieces starting from given piece index. */
  def nextPieces(startIndex: Int): IndexedSeq[TrackPiece] = nextPieceIndexes(startIndex).map(track.pieces(_))

  /** Get all next track pieces starting from my car current piece position. */
  def nextPieces: IndexedSeq[TrackPiece] = nextPieces(lastPos.pieceIndex)

  def indexOfSwitchpiece(startIndex: Int): Option[Int] = nextPieceIndexes(startIndex).find(track.pieces(_).hasSwitch)

  def getTick: Int = {
    assert(snapshots.length > 0)
    snapshots.length - 1
  }

  def velocity(tick: Int) = {
    if (tick < 1) 0
    else {
      val first = snapshots(tick - 1).positions(myColor)
      val second = snapshots(tick).positions(myColor)
      distance(first, second)
    }
  }

  def radialVelocity(tick: Int) = {
    if (tick < 1) 0
    else {
      val first = snapshots(tick - 1).positions(myColor)
      val second = snapshots(tick).positions(myColor)
      angleDiff(first, second)
    }
  }
  def angleDiff(tick: Int) = {
    if (tick < 1) 0
    else {
      val first = snapshots(tick - 1).positions(myColor)
      val second = snapshots(tick).positions(myColor)
      second.angle - first.angle
    }
  }

  /** Create new Position structure by offsetting previous one */
  def newPosition(old: Position, distAdd: Double, newAngle: Double): Position = {
    assert(distAdd >= 0, "Only forward supported")
    val newPos = old.inPieceDistance + distAdd
    val piece = track.pieces(old.pieceIndex)
    val pieceLen = length(piece, old.beginLane, old.endLane)
    if (newPos >= pieceLen) {
      // position is in next piece (assume continuing on end lane)
      Position(newAngle, (old.pieceIndex + 1) % numPieces, newPos - pieceLen, old.endLane, old.endLane, old.state)
    } else {
      Position(newAngle, old.pieceIndex, newPos, old.beginLane, old.endLane, old.state)
    }
  }

  def getPiecePos(pieceIndex: Int): V2d = {
    V2d(0, 0)
  }

  // ---- dump track info
  println("Lanes: " + track.lanes)
  println("Cars: " + track.cars)
  for (piece <- track.pieces.zipWithIndex) {
    println("Piece " + piece._2 + ": " + piece._1)
  }

}
