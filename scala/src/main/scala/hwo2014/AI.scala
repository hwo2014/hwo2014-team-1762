package hwo2014

import Math.abs
import Math.pow
import scala.util.Random

sealed abstract class Action
case class ActionThrottle(throttle: Double) extends Action
case class ActionSwitch(dir: Int) extends Action
case class ActionTurbo(msg: String) extends Action
case class ActionPing extends Action

abstract class AI {
  /** Initialize new race. */
  def gameInit(data: RaceData)
  /** Choose action for current race state. */
  def action(): Action
}

class DefaultAI(verbose: Boolean) extends AI {

  var data: RaceData = null
  val estimator = new SimpleEstimator
  var canChangeLane = true
  var lastLane = -1
  var lastAngle = 0.0
  var lastAngleSpeed = 0.0
  var lastThrottle: Double = 0.0
  
  var vis: Visualizer = null 
  var view: View = null
  
  def gameInit(data: RaceData) {
    reset()
    this.data = data
    estimator.init(data)
    if (verbose) {
	    vis = new Visualizer(data)
	    view = new View("Race", vis.image, 1)
    }
  }

  def reset()
  {
    data = null
    canChangeLane = true
    lastLane = -1
    lastAngle = 0.0
    lastAngleSpeed = 0.0
  }

  def action(): Action = {
    estimator.dataUpdated(data.getTick)
    if (verbose) {
      vis.visualize()
      view.repaint()
    }
    if (!estimator.validEstimates) throttle(1.0)
    else action1()
  }
  
  def log(message: String) = if (verbose) println(message)
  def throttle(value: Double): Action = {
    lastThrottle = value
    new ActionThrottle(value)
  }

  lazy val pieceSpeeds = data.track.pieces.map((p: TrackPiece) => estimator.slipVelocity(1.0/p.curvature))

  def action1() : Action = 
  {
    val retVal = action3()
    log("throttle: " + retVal.toString())
    return retVal
  }
  def action3() : Action = 
  {
    // *******************
    // ** Current state **
    // *******************
    val track = data.track
    val pos = data.lastPos
    val thisPiece = track.pieces(pos.pieceIndex)
    val velocity = data.velocity(data.getTick)
    val nextIndex = (pos.pieceIndex + 1) % track.pieces.length
    val nextPiece = track.pieces(nextIndex)

    if (lastLane < 0)
      lastLane = pos.beginLane
    if (lastLane == pos.endLane)
      canChangeLane = true

    val angleSpeed = pos.angle-lastAngle
    val angleAccel = angleSpeed-lastAngleSpeed
    val deltaAngle = Math.abs(pos.angle) - Math.abs(lastAngle)

    lastAngle = pos.angle
    lastAngleSpeed = angleSpeed

    // *****************
    // ** Turbo logic **
    // *****************
    if (data.turboAvailable() && track.fastestSections.exists(_.range.contains(pos.pieceIndex)) &&
	// thisPiece.curvature < 0.005 && 
	// thisPiece.section.averageCurvature < 0.02 &&
	// thisPiece.isInstanceOf[StraightPiece] &&
	!thisPiece.hasSwitch &&
	Math.abs(pos.angle) < 7.5 &&
        thisPiece.curvature < 0.00001
	// pos.inPieceDistance/thisPiece.length(track.lanes.toIndexedSeq, pos.endLane) < 0.1
      ) {
      if (lastThrottle < 1.0 || velocity < 0.75*estimator.maxVel) {
	log("Setting max throttle for turbo activation...");
	return throttle(1.0)
      } else {
	log("Activating turbo...");
	return new ActionTurbo("To infinity and beyond...");
      }
    }

    log("delta: " + deltaAngle + ", angle: " + pos.angle + ", vel: " + velocity + ", curwa: " + nextPiece.curvature)

    // ********************
    // ** Lane selection **
    // ********************
    val newLane = thisPiece.section.next.shortestLane
    if (canChangeLane && 
        pos.endLane != newLane && 
        thisPiece.section.next.lanes(pos.endLane).length != thisPiece.section.next.lanes(newLane).length) {
      val dir = if(track.lanes(pos.endLane) < track.lanes(newLane)) 1 else -1
      canChangeLane = false
      lastLane += dir
      log("Switch: " + pos.endLane + " , dir: " + dir)
      return ActionSwitch(dir)
    }

    // TODO: Following values are used to increase speeds to allow the car angle to get past the low default
    //       would be better to calculate real values taken into account when slowing down coming to the bend
    //       and setting to full throttle at end of bend once car orientation (taking into account pos.angle)
    //       matches the exit direction
    val bestGuessSpeedFactorFull = 1.5
    val bestGuessSpeedFactorLimited = 1.0

    // ***********************
    // ** Velocity controls **
    // *********************** 
    var targetSpeed = bestGuessSpeedFactorFull*pieceSpeeds(pos.pieceIndex)
    var totalDistance = -pos.inPieceDistance
    var limitingDistance = totalDistance+thisPiece.length(track.lanes,pos.endLane)
    for(i <- pieceSpeeds.indices) {
      val index = (pos.pieceIndex+i) % track.pieces.length
      // TODO: Base estimate on actual planned driving lane once other cars are taken into account.
      totalDistance += track.pieces(index).length(track.lanes,track.pieces(index).section.shortestLane)
      val nextSpeed = bestGuessSpeedFactorLimited*pieceSpeeds(index)
      val breakDistance = estimator.downUntilVelocity(velocity,nextSpeed)
      if (breakDistance._1 >= totalDistance) {
        targetSpeed = Math.min(targetSpeed,velocity-(velocity-nextSpeed)/breakDistance._2)
        limitingDistance = totalDistance
        return throttle(0)
      }
    }
    val throttleValue = Util.clamp(estimator.throttleForVelocity(velocity,targetSpeed)/data.turboFactor)
    log("p: "+pos.pieceIndex+" velocity: "+velocity+" angle: "+pos.angle+" targetSpeed: "+targetSpeed+" limitDistance "+limitingDistance+" throttle: "+throttleValue+" turbo: "+data.turboFactor)
    return throttle(throttleValue)
  }
  
}
