package hwo2014

import java.io.File
import java.io.PrintWriter
import java.text.SimpleDateFormat
import java.util.Date

class DataLogger(path: File) {
  path.mkdirs()
  val format = new SimpleDateFormat("yyyyMMdd-HHmmss")
  val file = new File(path, "log-" + format.format(new Date()) + ".json")
  val out = new PrintWriter(file)

  def write(line: String) {
    out.write(line)
    out.println()
    out.flush()
  }
}
