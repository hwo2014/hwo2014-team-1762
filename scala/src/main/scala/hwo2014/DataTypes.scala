package hwo2014

class Car(
  val name: String,
  val color: String,
  val length: Double,
  val width: Double,
  val guidePos: Double)

/** Track data. */
class TrackData( 
  val pieces: IndexedSeq[TrackPiece],
  val lanes: IndexedSeq[Double],
  val cars: Map[String, Car],
  val myCar: Car,
  val sections: IndexedSeq[TrackSection]) {

  lazy val bestFastness = sections.maxBy(_.fastnessEstimate).fastnessEstimate
  lazy val fastestSections = sections.filter(_.fastnessEstimate >= 0.95*bestFastness)
}

sealed abstract class TrackPiece {
  val hasSwitch: Boolean
  var section: TrackSection = null
  var prev: TrackPiece = null
  var next: TrackPiece = null
  def length(lanes: IndexedSeq[Double],laneIndex: Int): Double
  def angle: Double
  def radius: Double
  def curvature: Double
}
case class StraightPiece(hasSwitch: Boolean, length: Double) extends TrackPiece {
  def length(lanes: IndexedSeq[Double],laneIndex: Int) = length
  val angle = 0.0
  val curvature = 0.0
  val radius = Double.PositiveInfinity
}

case class BendPiece(hasSwitch: Boolean, val radius: Double, val angle: Double) extends TrackPiece {
  def length(lanes: IndexedSeq[Double],laneIndex: Int) = Math.abs(angle)/180.0*Math.PI*(radius-Math.signum(angle)*lanes(laneIndex))
  val curvature = 1 / radius
}

object CarState extends Enumeration {
  type CarState = Value
  val Active, Crashed, Finished = Value 
}

/** Car position in track. */
case class Position(angle: Double, pieceIndex: Int, inPieceDistance: Double, beginLane: Int, endLane: Int, state: CarState.Value)

/** ... */
class SectionLane (
  val length: Double,
  val curvature: Double
)

/** Section of the track between start/switch, end/switch */
class TrackSection (
  val range: IndexedSeq[Int],
  val lanes: IndexedSeq[SectionLane],
  var prev: TrackSection = null,
  var next: TrackSection = null
) {
  lazy val shortestLane: Int = lanes.zipWithIndex.minBy(_._1.length)._2
  lazy val averageLength = lanes.map(_.length).sum/lanes.length
  lazy val averageCurvature = lanes.map(_.curvature).sum/lanes.length
  def containsPiece(index: Int) = range.contains(index)
  lazy val fastnessEstimate = averageLength*(1.0-averageCurvature)
}

class Snapshot(
    val lastThrottle: Double,
    val positions: Map[String, Position] 
)

