package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{ BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter }
import org.json4s.native.Serialization
import scala.annotation.tailrec
import java.io.File

object NoobBot extends App {
  println("Args: " + args.toList)
  args.toList match {
    case host :: port :: botName :: botKey :: _ =>
      val protocol = BotMain.create(host, Integer.parseInt(port), botName, botKey, "usa")
      new BotMain(protocol, new DefaultAI(true))
    case _ => println("args missing")
  }
}

object BotMain {

  /** Connect to server without sending any init messages. */
  def connect(host: String, port: Int, logging: Boolean = false): Protocol = {
    val socket = new Socket(host, port)
    val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
    val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
    new Protocol(reader, Some(writer), if (logging) Some(new DataLogger(new File("racedata"))) else None)
  }

  /** Connect to server and send join message. */
  def join(host: String, port: Int, botName: String, botKey: String): Protocol = {
    val protocol = connect(host, port)
    protocol.send(MsgWrapper("join", MsgJoin(botName, botKey)))
    protocol
  }

  /** Connect to server and create and join to new track. */
  def create(host: String, port: Int, botName: String, botKey: String, trackName: String): Protocol = {
    val protocol = connect(host, port)
    val json = Map("name" -> botName, "key" -> botKey)
    protocol.send(MsgWrapper("createRace", MsgJoinRace(json, trackName, 1, "")))
    protocol.send(MsgWrapper("joinRace", MsgJoinRace(json, trackName, 1, "")))
    protocol
  }
}

class BotMain(val protocol: Protocol, val ai: AI) {
  var raceData: RaceData = null
  var lastThrottle: Double = 0

  var running = true
  while (running) protocol.processInput {
    case MsgGameInit(trackData: TrackData) =>
      raceData = new RaceData(trackData)
      ai.gameInit(raceData)
    case MsgCarPositions(positions: Map[String, Position], gameTick: Int) =>
      raceData.collect(lastThrottle, positions.toMap, gameTick)
      ai.action match {
        case ActionThrottle(throttle) =>
          lastThrottle = throttle
          protocol.send(MsgWrapper("throttle", throttle))
        case ActionSwitch(dir) =>
          protocol.send(MsgWrapper("switchLane", if (dir < 0) "Left" else "Right"))
        case ActionTurbo(message) =>
          protocol.send(MsgWrapper("turbo", message))
        case ActionPing() =>
          protocol.send(Map(("msgType", "ping")))
      }
    case MsgTournamentEnd() =>
      println("MsgTournamentEnd")
      running = false
    case MsgGameStart()                           => println("MsgGameStart")
    case MsgGameEnd(data)                         => println("MsgGameEnd: " + data)
    case MsgCrash(carColor: String)               => println("MsgCrash: " + carColor)
    case MsgSpawn(carColor: String)               => println("MsgSpawn: " + carColor)
    case MsgLapFinished(data: JValue)             => println("MsgLapFinished: " + data)
    case MsgDNF(carColor: String, reason: String) => println("MsgDNF: " + carColor)
    case MsgFinish(carColor: String)              => println("MsgFinish: " + carColor)
    case MsgTurboStart(name: String,color: String,gameTick: Int) => 
      // println("MsgTurboStart")
      raceData.beginTurbo(color)
    case MsgTurboEnd(name: String,color: String,gameTick: Int) => 
      // println("MsgTurboEnd")
      raceData.endTurbo(color)
    case MsgTurboAvailable(millis: Double, ticks: Int, factor: Double) =>
      // println("MsgTurboAvailable: millis: " + millis + ", ticks: " + ticks + ", factor: " + factor)
      raceData.addTurbo(factor,ticks);
    case MsgUndefined(msgType: String, value: JValue) => println("MsgUndefined " + msgType + ", " + value)
    case MsgEOF()                                     => assert(false, "Unexpected end of stream")
  }
}

