package hwo2014

object V2d {
  def apply(x: Double, y: Double) = new V2d(x, y)
}

/** 2-dimensional double vector. */
class V2d(val x: Double, val y: Double) {
  def rot: V2d = V2d(-y, x)
  def rot(angleRads: Double): V2d = {
    val cos = Math.cos(angleRads)
    val sin = Math.sin(angleRads)
    V2d(cos * x - sin * y, sin * x + cos * y)
  }
  def unary_-(): V2d = V2d(-x, -y)
  def +(s: Double): V2d = V2d(x + s, y + s)
  def +(i: V2d): V2d = V2d(x + i.x, y + i.y)
  def -(s: Double): V2d = V2d(x - s, y - s)
  override def toString: String = "[" + x + ", " + y + "]"
}
