package hwo2014

object MoonBot extends App {
  args.toList match {
    case host :: port :: botName :: botKey :: _ =>
      val protocol = BotMain.join(host, Integer.parseInt(port), botName, botKey)
      // val protocol = BotMain.create(host, Integer.parseInt(port), botName, botKey, "germany")
      new BotMain(protocol, new DefaultAI(false))
    case _ => println("args missing")
  }
}
